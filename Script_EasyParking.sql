CREATE DATABASE EasyParking;
USE EasyParking;

CREATE TABLE cliente
(
	idclientePK INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    apellido VARCHAR(50) NOT NULL,
    cedula int not null,
    telefono int not null

);

DROP TABLE cliente;

CREATE TABLE vehiculo
(
	placa varchar(50) not null,
    tipoVehiculo varchar(50) not null,
    fecha date not null,
    horaEntrada time not null,
    horaSalida time not null,
    idclienteFK int not null
);

DROP TABLE vehiculo;

insert into cliente values
(null, "Sebastian", "Ortiz", 112233, 313872),
(null, "Tannia", "Camargo", 1122334, 3138722),
(null, "Stefanny", "Martinez", 1122335, 3138725);

insert into	vehiculo(placa, tipoVehiculo, fecha, horaEntrada, horaSalida, idclienteFk)
values ("GPO210", "automovil", '2021/09/19', '10:20', '12:20', 1), ("GPO220", "Moto", '2021/09/18', '10:21', '12:32', 2), ("GDO610", "automovil", '2021/09/19', '10:40', '11:32', 3);

